<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <?php require "./lib/util.php"; writeTitleMetaTags(); ?>
        <?php getStylesheets(true, true, true, true, false, false); ?>
    </head>
    <body>
        <script type="text/javascript">
            document.addEventListener('touchmove', function(event) {
                event.preventDefault();
            }, false);
        </script>

        <br />
        <div id="head" class="spacer"></div>
        <div id="fade">
            <h1 class="header">sign.up</h1>
            <div class="errorMessage" style="margin-bottom: 4%;"><?php
                $status = $_GET["pass"];

                if($status === "email") {
                    echo("Email address already taken.");
                }

                if($status === "password") {
                    echo("Passwords entered did not match.");
                }
            ?></div>
            <form class="accountDetail" method="POST" action="/cgi-bin/new_user.php">
                <span class="formItem">
                    <label for="inp_name">Your Name</label><br />
                    <input type="text" name="NAME" id="inp_name" /><br />
                </span>
                <span class="formItem">
                    <label for="inp_email">Your Email Address</label><br />
                    <input type="email" name="EMAIL" id="inp_email" /><br />
                </span>
                <span class="formItem">
                    <label for="pw_init">Choose a Password</label><br />
                    <input type="password" name="PASSWORD_INIT" id="pw_init" /><br />
                </span>
                <div class="formItem" style="padding-bottom: 4%">
                    <label for="pw_conf">Enter It Again</label><br />
                    <input type="password" name="PASSWORD_CONF" id="pw_conf" /><br />
                </div>
                    <button type="submit">Sign Up!</button>
            </form>
            <form method="GET" action="/home.php">
                <button type="submit">Go Back</button>
            </form>
        </div>
        <img src="/media/curly-leaves.png" id="homepage_img" />

        <script type="text/javascript">
            var element = document.getElementById("fade");
            var duration = 1800;

            function SetOpa(Opa) {
                element.style.opacity = Opa;
                element.style.MozOpacity = Opa;
                element.style.KhtmlOpacity = Opa;
                element.style.filter = 'alpha(opacity=' + (Opa * 100) + ');';
            }

            function fadeIn() {
                for (i = 0; i <= 1; i += 0.01) {
                    setTimeout("SetOpa(" + i +")", i * duration);
                }
            }
            fadeIn();
        </script>
    </body>
</html>
