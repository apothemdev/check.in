<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <?php require "../lib/util.php"; writeTitleMetaTags(); ?>
        <?php getStylesheets(true, true, true, true, false, false); ?>
    </head>
    <body>
        <script type="text/javascript">
            document.addEventListener('touchmove', function(event) {
                event.preventDefault();
            }, false);
        </script>

        <br />
        <div id="head" class="spacer"></div>
        <div id="fade">
            <h1 class="header" style="margin-bottom: 4%;">Oh Noes!</h1>
            <p class="errorMessage">An error occurred: Error 404</p>
            <p class="errorMessage">This usually occurs due to a broken link, or a bad browser URI.</p>
            <form method="GET" action="/home.php" style="position:fixed;bottom:2%">
                <button type="submit">Go Home</button>
            </form>
            <img src="/media/curly-leaves.png" id="homepage_img" />
        </div>

        <script type="text/javascript">
            var element = document.getElementById("fade");
            var duration = 1800;

            function SetOpa(Opa) {
                element.style.opacity = Opa;
                element.style.MozOpacity = Opa;
                element.style.KhtmlOpacity = Opa;
                element.style.filter = 'alpha(opacity=' + (Opa * 100) + ');';
            }

            function fadeIn() {
                for (i = 0; i <= 1; i += 0.01) {
                    setTimeout("SetOpa(" + i +")", i * duration);
                }
            }
            fadeIn();
        </script>
    </body>
</html>
