<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <?php require "../lib/util.php"; writeTitleMetaTags(); ?>
        <?php getStylesheets(false, true, true, true, false, false); ?>
    </head>
    <!-- REPLACE USER_NAME COOKIE WITH A UID COOKIE FOR SECURITY!!!!! -->
    <?php
        if(userIsLoggedIn()) {
            header("Location: http://checkin.cuccfree.com/app/dashboard.php");
            die();
        }
    ?>
    <body>
        <script type="text/javascript">
            document.addEventListener('touchmove', function(event) {
                event.preventDefault();
            }, false);
        </script>

        <br />
        <div id="head" class="spacer"></div>
        <div id="fade">
            <h1 class="header" style="margin-bottom: 4%;"><?php echo($_COOKIE['user_name']); ?></h1>
            <iframe  style="border: 0px solid white;" src="/app/stream.php?id=<?php echo($_COOKIE['user_id']); ?>" width="100%" height="55%"></iframe>
            <form method="GET" action="/app/check_in.php" style="position:fixed;bottom:15%">
                <button type="submit">Check In</button>
            </form>
            <form method="GET" action="/app/find.php" style="position:fixed;bottom:2%">
                <button type="submit">Find friends</button>
            </form>
            <img src="/media/curly-leaves.png" id="homepage_img" />
        </div>

        <script type="text/javascript">
            var element = document.getElementById("fade");
            var duration = 1800;

            function SetOpa(Opa) {
                element.style.opacity = Opa;
                element.style.MozOpacity = Opa;
                element.style.KhtmlOpacity = Opa;
                element.style.filter = 'alpha(opacity=' + (Opa * 100) + ');';
            }

            function fadeIn() {
                for (i = 0; i <= 1; i += 0.01) {
                    setTimeout("SetOpa(" + i +")", i * duration);
                }
            }
            fadeIn();
        </script>
    </body>
</html>
