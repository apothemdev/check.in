<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <?php require "./lib/util.php"; writeTitleMetaTags(); ?>
        <?php getStylesheets(true, true, true, true, false, false); ?>
    </head>
    <body>
        <script type="text/javascript">
            document.addEventListener('touchmove', function(event) {
                event.preventDefault();
            }, false);
        </script>

        <br />
        <div id="head" class="spacer"></div>
        <div id="fade">
            <h1 class="header">log.in</h1>
            <div class="errorMessage" style="margin-bottom: 8%;"><?php
                $status = $_GET["pass"];

                if($status === "no") {
                    echo("Invalid login information.");
                }
            ?></div>
            <form class="accountDetail" method="POST" action="/cgi-bin/log_in.php">
                <span class="formItem">
                    <label for="inp_email">Email Address</label><br />
                    <input type="email" name="EMAIL" id="inp_email"/><br />
                </span>
                <div class="formItem" style="padding-bottom: 8%">
                    <label for="pw_init">Password</label><br />
                    <input type="password" name="PASSWORD" id="pw_init" /><br />
                </div>
                    <button type="submit">Log In</button>
            </form>
            <form method="GET" action="/home.php">
                <button type="submit">Go Back</button>
            </form>
        </div>
        <img src="/media/curly-leaves.png" id="homepage_img" />

        <script type="text/javascript">
            var element = document.getElementById("fade");
            var duration = 1800;

            function SetOpa(Opa) {
                element.style.opacity = Opa;
                element.style.MozOpacity = Opa;
                element.style.KhtmlOpacity = Opa;
                element.style.filter = 'alpha(opacity=' + (Opa * 100) + ');';
            }

            function fadeIn() {
                for (i = 0; i <= 1; i += 0.01) {
                    setTimeout("SetOpa(" + i +")", i * duration);
                }
            }
            fadeIn();
        </script>
    </body>
</html>
