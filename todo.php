<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <?php require "./lib/util.php"; writeTitleMetaTags(); ?>
        <?php getStylesheets(false, true, true, true, true, false); ?>
    </head>
    <body>
        <script type="text/javascript">
            document.addEventListener('touchmove', function(event) {
                event.preventDefault();
            }, false);
        </script>

        <br />
        <div id="head" class="spacer"></div>
        <div id="fade">
            <h1 class="header" style="margin-bottom: 4%;">Planned Features</h1>

            <div class="postbutton"><div class="post">check.inTel (Intelligent checkin processing)<div class="username">Assigned to Josh T.</div></div></div>

            <div class="postbutton"><div class="post">Achievements system<div class="username">Assigned to Josh T.</div></div></div>

            <div class="postbutton"><div class="post">Private checkins<div class="username">Assigned to JOsh T.</div></div></div>
            <form method="GET" action="http://checkin.cuccfree.com/" style="position:fixed;bottom:2%">
                <button type="submit">Go to check.in</button>
            </form>
            <img src="/media/curly-leaves.png" id="homepage_img" />
        </div>

        <script type="text/javascript">
            var element = document.getElementById("fade");
            var duration = 1800;

            function SetOpa(Opa) {
                element.style.opacity = Opa;
                 element.style.MozOpacity = Opa;
                element.style.KhtmlOpacity = Opa;
                element.style.filter = 'alpha(opacity=' + (Opa * 100) + ');';
            }

            function fadeIn() {
                for (i = 0; i <= 1; i += 0.01) {
                    setTimeout("SetOpa(" + i +")", i * duration);
                }
            }
            fadeIn();
        </script>
    </body>
</html>
