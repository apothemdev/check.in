<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <title>Check.In</title>
        <meta charset="UTF-8" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="width=device-width, height=device-height, user-scalable=no" />
        <link rel="icon" type="image/x-icon" href="media/favicon/favicon-standard.ico" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="57x57" href="/media/favicon/favicon-ipod-touch.ico" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="/media/favicon/favicon-iphone-generation-4.ico" />
        <link rel="stylesheet" type="text/css" href="/css/viewport.css?<?php echo(mt_rand(10000000, 99999999)); ?>" />
    </head>
    <script type="text/javascript">
            document.addEventListener('touchmove', function(event) {
                event.preventDefault();
            }, false);
    </script>
    <frameset cols="100%" rows="100%" noresize>
            <frame allowTransparency="true" src="home.php" />
    </frameset>
</html>
